package featureflag

import (
	"testing"
)

func Test_checkStateOfFeature(t *testing.T) {
	type args struct {
		feature string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		{
			name: "Feature exists and is active Hello",
			args: args{
				feature: "Hello",
			},
			want: true,
		},
		{
			name: "Feature is misspelled helo",
			args: args{
				feature: "helo",
			},
			want: false,
		}, {
			name: "Feature world does not exist in available features",
			args: args{
				feature: "World",
			},
			want: false,
		}, {
			name: "Feature Houdy was found but is not active",
			args: args{},
			want: false,
		},
	}

	for _, tt := range tests {

		setTestAvailableFeatures()
		t.Run(tt.name, func(t *testing.T) {
			if got := checkStateOfFeature(tt.args.feature); got != tt.want {
				t.Errorf("checkStateOfFeature() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_initActiveFeatures(t *testing.T) {
	type args struct {
		featureName string
		envString   string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		{
			name: "Hello3 is activated",
			args: args{
				featureName: "Hello3",
				envString:   "Hello3",
			},
			want: true,
		},
		{
			name: "Hello4 in env Hello4, Hello5 is activated",
			args: args{
				featureName: "Hello4",
				envString:   "Hello4, Hello5",
			},
			want: true,
		},
		{
			name: "Hello5 in env Hello4, Hello5 is activated",
			args: args{
				featureName: "Hello5",
				envString:   "Hello4, Hello5",
			},
			want: true,
		},
	}

	for _, tt := range tests {
		setTestAvailableFeatures()
		t.Setenv("ACTIVATED_FEATURES", tt.args.envString)

		t.Run(tt.name, func(t *testing.T) {

			initActiveFeatures()
			if availableFeatures[tt.args.featureName].active != tt.want {

				t.Errorf("foundFeature is not %v, want %v", availableFeatures[tt.args.featureName].active, tt.want)

			}

		})
	}
}

func setTestAvailableFeatures() {
	availableFeatures["Hello"] = &feature{
		description: "Just says hello",
		active:      true,
	}
	availableFeatures["Houdy"] = &feature{
		description: "Just says houdy",
		active:      false,
	}

	availableFeatures["Hello3"] = &feature{
		description: "Just says Hello3",
		active:      false,
	}
	availableFeatures["Hello4"] = &feature{
		description: "Just says Hello4",
		active:      false,
	}
	availableFeatures["Hello5"] = &feature{
		description: "Just says Hello5",
		active:      false,
	}
}

func example() string {
	var output string
	if checkStateOfFeature("Hello3") {
		output = output + "Hello3"
	}

	if checkStateOfFeature("Hello4") {
		output = output + "Hello4"
	}

	return output
}

func Test_example(t *testing.T) {
	type args struct {
		envString string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "One flag 1: Hello3",
			args: args{
				envString: "Hello3",
			},
			want: "Hello3",
		}, {
			name: "Two flags: Hello3 Hello4",
			args: args{
				envString: "Hello3,Hello4",
			},
			want: "Hello3Hello4",
		}, {
			name: "Two flags: Hello4 Hello3: reversed no difference",
			args: args{
				envString: "Hello3,Hello4",
			},
			want: "Hello3Hello4",
		}, {
			name: "One flag 2: Hello4",
			args: args{
				envString: "Hello4",
			},
			want: "Hello4",
		},
		{
			name: "two identical flags: Hello4",
			args: args{
				envString: "Hello4,Hello4",
			},
			want: "Hello4",
		},
	}
	for _, tt := range tests {
		setTestAvailableFeatures()
		t.Setenv("ACTIVATED_FEATURES", tt.args.envString)

		initActiveFeatures()

		t.Run(tt.name, func(t *testing.T) {
			if got := example(); got != tt.want {
				t.Errorf("example() = %v, want %v", got, tt.want)
			}
		})
	}
}

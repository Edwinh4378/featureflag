package featureflag

import (
	"log"
	"os"
	"strings"
)

var availableFeatures = make(map[string]*feature)

type feature struct {
	description string
	active      bool
}

func initActiveFeatures() {

	featureflagsenv := os.Getenv("ACTIVATED_FEATURES")
	foundActiveFeatures := strings.Split(featureflagsenv, ",")

	for _, v := range foundActiveFeatures {
		parsedFeature := strings.TrimSpace(v)
		featureValue, ok := availableFeatures[parsedFeature]
		if ok {

			featureValue.active = true
		}

	}

	for key, v := range availableFeatures {

		log.Printf("featureTag : %s , %s is active ==> %v ", key, v.description, v.active)

	}

}

func checkStateOfFeature(feature string) bool {
	value, OK := availableFeatures[feature]

	if OK && value.active {
		log.Printf("Activated feature %s is triggered: ", feature)
	} else if !OK {
		log.Printf("FeatureTag : %s, is not a known featuretag", feature)
	}

	return OK && value.active
}

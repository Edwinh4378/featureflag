Dev container with pipeline


pull repository with gitbash 

git clone git@bitbucket.org:Edwinh4378/auth_mock.git
git clone -b develop --single-branch git@bitbucket.org:Edwinh4378/auth_mock.git

In powershell

ssh-add $HOME/.ssh/github_rsa

Make sure you're running as an Administrator
Set-Service ssh-agent -StartupType Automatic
Start-Service ssh-agent
Get-Service ssh-agent

in vs code

f1
open remote: open folder in container
f1
install the go tools

in container terminal
git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
add variable: go env -w GOPRIVATE="bitbucket.org/Edwinh4378/*"

pipeline needs te be and needs credentials